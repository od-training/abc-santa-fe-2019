import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { VideoDashboardComponent } from './video-dashboard/video-dashboard.component';
import { StatFiltersComponent } from './stat-filters/stat-filters.component';
import { VideoListComponent } from './video-list/video-list.component';
import { VideoPlayerComponent } from './video-player/video-player.component';
import { VideoDisplayComponent } from './video-display/video-display.component';

const routes: Routes = [{
  path: '',
  component: VideoDashboardComponent
}];

@NgModule({
  declarations: [VideoDashboardComponent, StatFiltersComponent, VideoListComponent, VideoPlayerComponent, VideoDisplayComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
