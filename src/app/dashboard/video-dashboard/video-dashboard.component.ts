import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { filter, map, shareReplay, switchMap } from 'rxjs/operators';
import { Video } from '../video';
import { VideoDataService } from '../video-data.service';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {
  videos: Observable<Video[]>;
  selectedVideo: Observable<Video>;

  constructor(private videoSvc: VideoDataService,
              private route: ActivatedRoute,
              private router: Router) {
    this.videos = videoSvc.loadVideos();

    this.selectedVideo = this.route.queryParamMap
      .pipe(
        map(paramMap => paramMap.get('selectedVideoId')),
        filter(videoId => !!videoId),
        switchMap(videoId => this.videoSvc.getVideo(videoId)),
        shareReplay(1)
      );

    // const selectedId = this.route.queryParamMap
    //   .pipe(
    //     map(paramMap => paramMap.get('selectedVideoId')),
    //     filter(videoId => !!videoId)
    //   );
    //
    // this.selectedVideo = combineLatest([this.videos, selectedId])
    //   .pipe(
    //     map(([videoList, id]) => videoList.find(video => video.id === id))
    //   );
  }

  selectVideo(id: string) {
    void this.router.navigate([], {
      queryParams: { selectedVideoId: id },
      queryParamsHandling: 'merge'
    });
  }

}
