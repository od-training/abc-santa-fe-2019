import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map, retry } from 'rxjs/operators';
import { Video } from './video';

const videoApi = 'http://api.angularbootcamp.com/videos';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {

  constructor(private http: HttpClient) { }

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(videoApi)
      .pipe(
        map(videos => videos.slice(1, 10))
      );
  }

  getVideo(id: string): Observable<Video> {
    return this.http.get<Video>(`${videoApi}/${id}`)
      .pipe(
        retry(2),
        catchError(e => {
          console.log(e);
          return of(null);
        })
      );
  }
}
