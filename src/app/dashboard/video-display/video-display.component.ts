import { Component, Input } from '@angular/core';
import { Video } from '../video';

@Component({
  selector: 'app-video-display',
  templateUrl: './video-display.component.html',
  styleUrls: ['./video-display.component.css']
})
export class VideoDisplayComponent {
  @Input() video: Video;

}
