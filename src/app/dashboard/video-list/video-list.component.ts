import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Video } from '../video';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {

  @Input() videos: Video[];
  @Input() selected: Video;

  @Output() videoWasClicked = new EventEmitter<Video>();

  videoClicked(video) {
    this.videoWasClicked.emit(video);
  }

}
